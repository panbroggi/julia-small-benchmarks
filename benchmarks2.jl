# Test of the sum of the square of an array in..

using BenchmarkTools

## Julia built-in
a = rand(10^7)
j_bench = @benchmark sum($a.^2)
d = Dict()
d["Julia built-in"] = minimum(j_bench.times) / 1e6

## Julia hand-written
function mysum(A)
    s = 0.0
    for a in A
        s += a^2
    end
    return s
end
j_bench_hand = @benchmark mysum($a)
d["Julia hand-written"] = minimum(j_bench_hand.times) / 1e6

## Julia hand-written with SIMD
function mysumSIMD(A)
    s = 0.0
    @simd for a in A
        s += a^2
    end
    return s
end
j_bench_hand_SIMD = @benchmark mysumSIMD($a)
d["Julia hand-written + SIMD"] = minimum(j_bench_hand_SIMD.times) / 1e6

## C
using Libdl
C_code = """
    #include <stddef.h>
    double c_sum(size_t n, double *X) {
        double s = 0.0;
        for (size_t i = 0; i < n; ++i) {
            s += (*X) * (*X);
            ++X;
        }
        return s;
    }
"""
const Clib = tempname()   # make a temporary file
# compile to a shared library by piping C_code to gcc
# (works only if you have gcc installed):
open(`gcc -fPIC -O3 -msse3 -xc -shared -o $(Clib * "." * Libdl.dlext) -`, "w") do f
    print(f, C_code)
end
# define a Julia function that calls the C function:
c_sum(X::Array{Float64}) = ccall(("c_sum", Clib), Float64, (Csize_t, Ptr{Float64}), length(X), X)
c_bench = @benchmark c_sum($a)
d["C"] = minimum(c_bench.times) / 1e6  # in milliseconds


## Python built-in
using PyCall
py"""
def py_sum_b(A):
    return sum(A*A)
"""
pysum = py"py_sum_b"
py_list_bench = @benchmark $pysum($a)

d["Python built-in"] = minimum(py_list_bench.times) / 1e6

## Python hand-written
py"""
def py_sum(A):
    s = 0.0
    for a in A:
        s += a*a
    return s
"""
sum_py = py"py_sum"
py_hand = @benchmark $sum_py($a)
d["Python hand-written"] = minimum(py_hand.times) / 1e6

## Python numpy
py"""
import numpy as np
def np_sum(A):
    return np.sum(A**2)
"""
sum_np = py"np_sum"
py_np = @benchmark $sum_np($a)
d["Python numpy"] = minimum(py_np.times) / 1e6


## Print output
for (key, value) in sort(collect(d), by=last)
    println(rpad(key, 25, "."), lpad(round(value; digits=1), 6, "."))
end
