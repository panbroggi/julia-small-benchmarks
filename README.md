# Julia small benchmarks

Simple benchmarks to compare `C`, `Python+numpy` and `Julia`.

To use it on your machine, you need
* a `julia` installation with the following packages
  - `Benchmarks.jl`
  - `PyCall.jl`

* a `gcc` installation
* a `Python` installation with `numpy`

Tu be shure to run on single thread, launch the command as
```
taskset --cpu-list 1 julia benchmarks.jl
```

## Benchmark 1
`sum(a) # a is an array of 10^7 elements`

On my machine `julia benchmarks.jl` gives (time in milliseconds)
```
Python numpy................5.3
Julia hand-written + SIMD...5.8
Julia built-in..............5.8
Julia hand-written.........12.0
C..........................12.6
Python built-in...........694.7
Python hand-written.......820.6
```

## Benchmark 2
`sum(a^2) # a is an array of 10^7 elements`

In the same conditions,`julia benchmarks2.jl` gives
```
Julia hand-written + SIMD...5.9
C..........................14.2
Julia hand-written.........14.2
Python numpy...............23.2
Julia built-in.............24.5
Python built-in...........767.2
Python hand-written......1338.5
```

## Benchmark 3
`x' * y # x and y are arrays of dimension n`

Running `julia benchmarks3.jl` gives
```
n =10 :  17.891 ns (0 allocations: 0 bytes)
n =100 :  23.994 ns (0 allocations: 0 bytes)
n =1000 :  90.277 ns (0 allocations: 0 bytes)
n =10000 :  1.636 μs (0 allocations: 0 bytes)
n =100000 :  8.374 μs (0 allocations: 0 bytes)
n =1000000 :  482.529 μs (0 allocations: 0 bytes)
n =10000000 :  7.340 ms (0 allocations: 0 bytes)
```
and `ipython benchmarks3.ipy` gives
```
n = 10 : 952 ns ± 33.1 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
n = 100 : 974 ns ± 44.8 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
n = 1000 : 1.11 µs ± 16.1 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
n = 10000 : 3.12 µs ± 100 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
n = 100000 : 17.4 µs ± 476 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
n = 1000000 : 810 µs ± 44.6 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)
n = 10000000 : 12.7 ms ± 155 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)
```

## Benchmark 4
`sum(x.*y) # x and y are arrays of dimension n`

Running `julia benchmarks4.jl` gives
```
n =10 :  41.493 ns (1 allocation: 160 bytes)
n =100 :  101.549 ns (1 allocation: 896 bytes)
n =1000 :  620.932 ns (1 allocation: 7.94 KiB)
n =10000 :  6.244 μs (2 allocations: 78.20 KiB)
n =100000 :  102.185 μs (2 allocations: 781.33 KiB)
n =1000000 :  2.093 ms (2 allocations: 7.63 MiB)
n =10000000 :  27.474 ms (2 allocations: 76.29 MiB)
```
and `ipython benchmarks4.ipy` gives
```
n = 10 : 3.19 µs ± 125 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
n = 100 : 3.77 µs ± 260 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
n = 1000 : 4.95 µs ± 212 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
n = 10000 : 14.9 µs ± 770 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)
n = 100000 : 96.8 µs ± 1.88 µs per loop (mean ± std. dev. of 7 runs, 10000 loops each)
n = 1000000 : 2.09 ms ± 25.3 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)
n = 10000000 : 27.2 ms ± 1.15 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)
```

## Details of my setup
System: Intel core i7-8550U with KaOS Linux

`Python` version: 3.8.9 (gcc 10.2.0)

`Julia` version: 1.6.1

`gcc` version: 10.3.0

## Resources

The idea and the initial version of the scripts is inspired by the [Introduction to Julia programming](https://eventi.cineca.it/en/hpc/introduction-julia-programming-language-0) course held by the HPC Cineca Academy. Other hints, in particular towards understanding how numpy and julia are faster than C in some cases, come from [this Jupyter notebook](https://github.com/mitmath/18S096/blob/master/lectures/lecture1/Boxes-and-registers.ipynb). The fourth benchmark comes from [here](https://github.com/kbarbary/website/blob/master/posts/julia-vs-numpy-arrays.rst) and inspired the third - which uses the simple Julia way instead of a new routine.
