# Test of the sum of the square of an array in..
using BenchmarkTools
for n in [10, 100, 1000, 10000, 100000, 1000000, 10000000]
    x = rand(n)
    y = rand(n)
    print("n =$n :")
    @btime $x'*$y
end
