using BenchmarkTools
for n in [10, 100, 1000, 10000, 100000, 1000000, 10000000]
    x = rand(n)
    y = rand(n)
    print("n =$n :")
    @btime sum($x.*$y)
end
